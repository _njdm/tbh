#ifndef DEBUGGING_H
#define DEBUGGING_H 1

#include <stdio.h>

#ifdef DEBUG
#define LOG(...) do { fprintf(stderr, __VA_ARGS__); } while (0)
#else
#define LOG(...) do {} while (0)
#endif

#endif /* DEBUGGING_H */
