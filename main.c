#include "tokenizer.h"

#include "debugging.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(int argc, char *argv[])
{
    FILE *file = NULL;

    if (argc < 2)
    {
        printf("Usage: %s <PROGRAM>\n", argv[0]);
        return 0;
    }

    if ((file = fopen(argv[1], "rb")))
    {
        unsigned int        size     = 0;
        unsigned int        length   = 0;
        unsigned char      *buffer   = NULL;
        const unsigned int  readsize = 512;

        while (!feof(file))
        {
            size += readsize;
            buffer = realloc(buffer, sizeof(char) * size);
            length += fread((buffer + size) - 512,
                            1,
                            readsize,
                            file);
        }

        if (fclose(file))
        {
            fprintf(stderr, "Error closing file %s", argv[1]);
            if (buffer) free(buffer);
            return -1;
        }

        if (!buffer) return -1;
        buffer[length] = '\0';

        token_list_t *tokens = scan_buffer(buffer, length);
        destroy_token_list(tokens);
    }

    return 0;
}
