#include "tokenizer.h"

#include "debugging.h"

#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

token_t *
create_token(char *image, token_type_t type)
{
    token_t *ret = malloc(sizeof(*ret));
    if (!ret)
    {
        perror("malloc");
        return NULL;
    }

    if (image)
    {
        ret->image = malloc(sizeof(char) * strlen(image));
        if (!ret->image)
        {
            free(ret);
            perror("malloc");
            return NULL;
        }

        strcpy(ret->image, image);

        LOG("token(%s, %d)\n", image, type);
    }
    else
    {
        ret->image = NULL;

        LOG("token(%d)\n", type);
    }
    ret->type = type;

    return ret;
}

void
destroy_token(token_t *token)
{
    if (!token) return;

    free(token->image);
    free(token);
}

token_list_t *
create_token_list(void)
{
    token_list_t *ret = malloc(sizeof(*ret));
    if (!ret)
    {
        perror("malloc");
        return NULL;
    }

    ret->num = 0;
    ret->tokens = NULL;

    return ret;
}

void
destroy_token_list(token_list_t *list)
{
    unsigned int n;

    if (!list) return;

    for (n = 0; n < list->num; ++n)
    {
        destroy_token(list->tokens[n]);
    }

    free(list->tokens);
    free(list);
}

bool
token_list_add_token(token_list_t *list, token_t *token)
{
    unsigned int  newsize;
    void         *mem = NULL;

    if (!list) return false;

    newsize = list->num + 1;
    mem = realloc(list->tokens, sizeof(token_t *) * newsize);
    if (!mem)
    {
        perror("realloc");
        return false;
    }

    list->tokens = mem;
    list->tokens[list->num] = token;
    list->num = newsize;

    return true;
}

token_list_t *
scan_buffer(unsigned char *buffer, unsigned int size)
{
    const unsigned char *start = buffer;
    token_list_t        *list  = NULL;
    unsigned char temp[2]      = {0};

    list = create_token_list();
    if (!list) return NULL;

    while (start < buffer + size)
    {

        // Whitespace
        if (*start == 0xaa)
        {
            ++start;
            continue;
        }

        // Assignment
        if (*start == 0x10)
        {
            token_t *token = create_token(NULL, TKT_ASSIGNMENT);
            if (!token_list_add_token(list, token))
            {
                destroy_token(token);
                destroy_token_list(list);
                return NULL;
            }

            ++start;
            continue;
        }

        // Number literal (normal)
        if (*start <= 0x09)
        {
            token_t       *token       = NULL;
            unsigned char *num         = NULL;
            size_t         buffer_size = 4;
            size_t         digits      = 0;

            while (*start <= 0x09)
            {

            }

            ++start;
            continue;
        }

        // Variable
        if (*start >= 0x0a && *start <= 0x1f)
        {
            token_t *token = NULL;

            temp[0] = *start;
            token = create_token((char *)temp, TKT_VARIABLE);
            if (!token_list_add_token(list, token))
            {
                destroy_token(token);
                destroy_token_list(list);
                return NULL;
            }

            ++start;
            continue;
        }

        // Function
        {
            token_t *token = NULL;

            temp[0] = *start;
            token = create_token((char *)temp, TKT_FUNCTION);
            if (!token_list_add_token(list, token))
            {
                destroy_token(token);
                destroy_token_list(list);
                return NULL;
            }

            ++start;
            continue;
        }
    }

    // Implicit exit
    token_list_add_token(list, create_token("\xff", TKT_FUNCTION));

    return list;
}
