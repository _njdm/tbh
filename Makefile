CC=gcc
DEBUG=
CFLAGS=-Wall -c -std=c11 -O2 $(DEBUG)
LDFLAGS=-Wall $(DEBUG)
SOURCES=main.c tokenizer.c
OBJECTS=$(SOURCES:.cpp=.o)
EXECUTABLE=tbh.exe

all: $(SOURCES) $(EXECUTABLE)

debug: DEBUG+=-g -DDEBUG
debug: all

$(EXECUTABLE): $(OBJECTS)
	$(CC) $(LDFLAGS) $(OBJECTS) -o $@

.cpp.o:
	$(CC) $(CFLAGS) $< -o $@

clean:
	find . -name "*~" -type f -delete
	find . -name "*.o" -type f -delete
