#ifndef TOKENIZER_H
#define TOKENIZER_H 1

#include <stdbool.h>

/**
 * Represents the possible types of a token.
 */
typedef enum token_type
{
    TKT_NUMBER,
    TKT_STRING,
    TKT_FUNCTION,
    TKT_VARIABLE,
    TKT_ASSIGNMENT,
} token_type_t;

/**
 * Respresents one specific type of unit.
 */
typedef struct token
{
    char *image;
    token_type_t type;
} token_t;

/**
 * Represents a list of tokens.
 */
typedef struct token_list
{
    unsigned int num;
    token_t **tokens;
} token_list_t;

token_t *create_token(char *, token_type_t);
void destroy_token(token_t *);

token_list_t *create_token_list(void);
void destroy_token_list(token_list_t *);

bool token_list_add_token(token_list_t *, token_t *);

token_list_t *scan_buffer(unsigned char *, unsigned int);

#endif /* TOKENIZER_H */
